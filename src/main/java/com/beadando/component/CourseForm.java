package com.beadando.component;

import com.beadando.entity.Course;
import com.beadando.entity.Room;
import com.beadando.repository.CourseRepository;
import com.beadando.repository.RoomRepository;
import com.beadando.view.Reloader;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@UIScope
public class CourseForm extends VerticalLayout {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private RoomRepository roomRepository;
    private Course course;
    private Reloader reloader;
    private TextField name;
    private Binder<Course> binder;

    @PostConstruct
    public void init(){
        binder = new Binder<>(Course.class);

        name = new TextField("Name");

        Button deleteButton = new Button();
        deleteButton.setText("Delete");
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        deleteButton.addClickListener(buttonClickEvent -> {
            courseRepository.delete(course);
            reloader.reload();
            Notification.show("Room removed");
            setVisible(false);
        });

        Button editButton = new Button();
        editButton.setText("Save");
        editButton.setIcon(VaadinIcon.PENCIL.create());
        editButton.addClickListener(buttonClickEvent -> {
            courseRepository.update(course);
            reloader.reload();
            Notification.show("Succesful edit");
            setVisible(false);
        });

        add(name);
        add(editButton);
        add(deleteButton);
        setVisible(false);
        binder.bindInstanceFields(this);

        ComboBox<Room> comboBox = new ComboBox<>();
        comboBox.setPlaceholder("Termek");
        comboBox.setItems(roomRepository.findAll());
        comboBox.setItemLabelGenerator(Room::getName);
        Button addButton = new Button("Add");
        addButton.setIcon(VaadinIcon.PLUS.create());
        addButton.addClickListener( clickEvent -> {
            courseRepository.addRoom(course, comboBox.getValue());
            reloader.reload();
            Notification.show(" " +comboBox.getValue().getName());
        });
        add(new Label("Termek"));
        add(comboBox);
        add(addButton);
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course){
        if (course != null){
            this.course = course;
            binder.setBean(this.course);
        }
        this.course = course;
    }

    public Reloader getReloader() {
        return reloader;
    }

    public void setReloader(Reloader reloader){
        this.reloader = reloader;
    }
}
