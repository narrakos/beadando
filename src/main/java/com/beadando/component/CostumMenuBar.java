package com.beadando.component;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class CostumMenuBar extends HorizontalLayout {

    public CostumMenuBar(){
        addMenuItem("rooms", "Rooms");
        addMenuItem("courses", "Courses");

    }

    public void addMenuItem(String href, String text){
        Anchor link = new Anchor();
        link.setHref(href);
        link.setText(text);
        add(link);
    }
}
