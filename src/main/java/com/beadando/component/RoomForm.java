package com.beadando.component;

import com.beadando.entity.Course;
import com.beadando.entity.Room;
import com.beadando.repository.CourseRepository;
import com.beadando.repository.RoomRepository;
import com.beadando.view.Reloader;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;



@SpringComponent
@UIScope
public class RoomForm extends VerticalLayout {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private CourseRepository courseRepository;
    private Room room;
    private Reloader reloader;
    private TextField name;
    private Binder<Room> binder;

    @PostConstruct
    public void init(){
        binder = new Binder<>(Room.class);

        name = new TextField("Name");

        Button deleteButton = new Button();
        deleteButton.setText("Delete");
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        deleteButton.addClickListener(buttonClickEvent -> {
            roomRepository.delete(room);
            reloader.reload();
            Notification.show("Room removed");
            setVisible(false);
        });

        Button editButton = new Button();
        editButton.setText("Save");
        editButton.setIcon(VaadinIcon.PENCIL.create());
        editButton.addClickListener(buttonClickEvent -> {
            roomRepository.update(room);
            reloader.reload();
            Notification.show("Succesful edit");
            setVisible(false);
        });


        add(name);
        add(editButton);
        add(deleteButton);
        setVisible(false);
        binder.bindInstanceFields(this);
    }

    public Room getRoom() {
        return room;
    }

    public void addCourse(Room room, Course course){
        course.setRoom(room);
    }

    public void setRoom(Room room) {
        if (room != null){
            this.room = room;
            binder.setBean(this.room);
        }
        this.room = room;
    }

    public Reloader getReloader() {
        return reloader;
    }

    public void setReloader(Reloader reloader){
        this.reloader = reloader;
    }
}
