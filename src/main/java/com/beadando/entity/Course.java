package com.beadando.entity;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(name = "Course.FIND_BY_ROOM", query = "select n from Course n where n.room=:room")
})
@Entity
@Table
public class Course {

    public static final String FIND_BY_ROOM = "Course.FIND_BY_ROOM";

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int Id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;

    public Course(){}

    public Course(String name) {
        this.name = name;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
