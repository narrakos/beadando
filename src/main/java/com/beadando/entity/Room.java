package com.beadando.entity;


import javax.persistence.*;



@NamedQuery(name = "Room.FIND_ALL", query = "select n from Room n")
@Entity
@Table
public class Room {

    public static final String FIND_ALL = "Room.findAll";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private String name;
    private int level;


    public Room(){}

    public Room(int id, String name, int level) {
        this.Id = id;
        this.name = name;
        this.level = level;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

}
