package com.beadando.repository;


import com.beadando.entity.Course;
import com.beadando.entity.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class CourseRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public void update(Course course){
        entityManager.merge(course);
    }

    public void save(Course course){
        entityManager.persist(course);
    }

    public List<Course> findAll(){
        return entityManager.createQuery("SELECT n from " + Course.class.getSimpleName() + " n").getResultList();
    }

    public Course findById(int id){
        return entityManager.find(Course.class, id);
    }

    public void delete(Course course){
        entityManager.remove(findById(course.getId()));
    }

    public void addRoom(Course course, Room room){
        course.setRoom(room);
        update(course);
    }

    public List<Course> listByRoom(Room room){
        return entityManager.createNamedQuery(Course.FIND_BY_ROOM)
                .setParameter("room", room).getResultList();

    }
}
