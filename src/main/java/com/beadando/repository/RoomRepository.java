package com.beadando.repository;


import com.beadando.entity.Course;
import com.beadando.entity.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;



@Transactional
@Repository
public class RoomRepository {



    @PersistenceContext
    private EntityManager entityManager;

    public void update(Room room){
        entityManager.merge(room);
    }

    public void save(Room room){
        entityManager.persist(room);
    }

    public List<Room> findAll(){
        //return entityManager.createNamedQuery(Room.FIND_ALL).getResultList();
        return entityManager.createQuery("select n from " + Room.class.getSimpleName() + " n").getResultList();


    }

    public Room findById(int id){
        return entityManager.find(Room.class, id);
    }

    public void delete(Room room){
        entityManager.remove(findById(room.getId()));
    }

    public void addCourse(Room room, Course course){
        course.setRoom(room);
    }


}
