package com.beadando.view;

import com.beadando.component.CostumMenuBar;
import com.vaadin.flow.router.Route;

@Route
public class MainView extends AbstractView {

    public MainView(){
        add(new CostumMenuBar());
    }

}
