package com.beadando.view;

import com.beadando.component.CostumMenuBar;
import com.beadando.component.CourseForm;
import com.beadando.entity.Course;
import com.beadando.entity.Room;
import com.beadando.repository.CourseRepository;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route
public class CoursesView extends VerticalLayout implements Reloader {

    @Autowired
    private CourseRepository repository;

    @Autowired
    private CourseForm form;

    Grid<Course> grid = new Grid<>();

    @PostConstruct
    public void init(){
        add(new CostumMenuBar());
        List<Course> list = repository.findAll();

        if (list.isEmpty()){
            for (int i = 0; i < 10; i++) {
                Course course = new Course();
                course.setName("Kurzus " + i);
                repository.save(course);
            }
            list = repository.findAll();
        }

        grid.setItems(list);
        grid.addColumn(Course::getName).setHeader("Név");
        grid.addColumn(Course::getId).setHeader("Id");
        grid.addColumn(new ComponentRenderer<>( course ->{
            Button aboutButton = new Button("About");
            aboutButton.addClickListener( clickEvent ->{
                UI.getCurrent().navigate("course/view/" + course.getId());
            });
            return aboutButton;
        }));



        grid.asSingleSelect().addValueChangeListener(gridRoomComponentValueChangeEvent -> {
            form.setVisible(true);
            form.setCourse(gridRoomComponentValueChangeEvent.getValue());
            form.setReloader(this);
        });

        Button button = new Button();
        button.setText("Add");
        button.setIcon(VaadinIcon.PLUS.create());
        button.addClickListener(buttonClickEvent -> {
            form.setVisible(true);
            form.setReloader(this);
            form.setCourse(new Course());
        });

        add(grid);
        add(button);
        add(form);
    }

    @Override
    public void reload() {
            grid.setItems(repository.findAll());
    }
}
