package com.beadando.view;

import com.beadando.component.CostumMenuBar;
import com.beadando.component.RoomForm;
import com.beadando.entity.Course;
import com.beadando.entity.Room;
import com.beadando.repository.CourseRepository;
import com.beadando.repository.RoomRepository;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@Route(value = "room/view")
public class RoomPageView extends VerticalLayout implements HasUrlParameter<String> {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private CourseRepository courseRepository;
    private Room room;
    private VerticalLayout roomInfo;
    private Grid<Room> roomGrid;
    private Grid<Course> courseGrid;


    @Override
    public void setParameter(BeforeEvent event, String parameter) {
        try {
            room = roomRepository.findById(Integer.parseInt(parameter));
            if (room != null){
                add(new CostumMenuBar());
                add(new Label("Név: " + room.getName()));
                add(new Label("Id: " + room.getId()));
                add(new Label("Szint: " + room.getLevel()));
                listCourses(room);
            } else {
                add(new Text("A terem nem létezik"));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void listCourses(Room room){
        List<Course> list = new ArrayList<>();
        try {
            list = courseRepository.listByRoom(room);
        } catch (Exception e){
            add(new Label("sql error"));
            e.printStackTrace();
        }
        courseGrid = new Grid<>();

        add(new Label("Kurzusok"));
        courseGrid.setItems(list);
        courseGrid.addColumn(Course::getName).setHeader("Name");
        courseGrid.addColumn(Course::getId).setHeader("Id");
        if (list.isEmpty()){
            add(new Text("Nincs kurzus"));
        } else  {
            add(courseGrid);
        }
    }




}
