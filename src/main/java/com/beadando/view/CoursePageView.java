package com.beadando.view;

import com.beadando.component.CostumMenuBar;
import com.beadando.component.CourseForm;
import com.beadando.entity.Course;
import com.beadando.repository.CourseRepository;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "course/view")
public class CoursePageView extends VerticalLayout implements HasUrlParameter<String> {

    @Autowired
    private CourseRepository repository;
    private Course course;


    @Override
    public void setParameter(BeforeEvent beforeEvent, String parameter) {
        try {
            course = repository.findById(Integer.parseInt(parameter));
            if (course != null){
                add(new CostumMenuBar());
                add(new Label("Név: " + course.getName()));
                add(new Label("Id: " + course.getId()));


                if (course.getRoom() != null){
                    add(new Label("Terem: " + course.getRoom().getName()));
                }else {
                    add(new Label("Nincs terem"));
                }

                add(new CourseForm());

            } else {
                add(new Text("A kurzus nem létezik"));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
