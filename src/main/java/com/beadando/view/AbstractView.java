package com.beadando.view;

import com.beadando.component.CostumMenuBar;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public abstract class AbstractView extends VerticalLayout {
    public void addMenuber(){
        add(new CostumMenuBar());
    }
}
