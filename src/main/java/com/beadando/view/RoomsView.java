package com.beadando.view;

import com.beadando.component.CostumMenuBar;
import com.beadando.component.RoomForm;
import com.beadando.entity.Course;
import com.beadando.entity.Room;
import com.beadando.repository.CourseRepository;
import com.beadando.repository.RoomRepository;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ClickableRenderer;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.router.Route;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Route
public class RoomsView extends VerticalLayout implements Reloader {

    @Autowired
    private RoomRepository repository;

    @Autowired
    private RoomForm form;

    Grid<Room> grid = new Grid<>();

    private ComboBox<Course> course;

    @Autowired
    private CourseRepository courseRepository;

    @PostConstruct
    public void init(){
        add(new CostumMenuBar());
        List<Room> list = repository.findAll();

        course = new ComboBox<>();
        course.setLabel("Kurzus");
        try {
            course.setItems(courseRepository.findAll());
            course.setItemLabelGenerator(Course::getName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (list.isEmpty()){
            for (int i = 0; i < 10; i++) {
                Room room = new Room();
                room.setLevel(i);
                room.setName("Terem neve: " + i);
                repository.save(room);
            }
            list = repository.findAll();
        }

        grid.setItems(list);
        grid.addColumn(Room::getName).setHeader("Név");
        grid.addColumn(Room::getId).setHeader("Id");
        grid.addColumn(Room::getLevel).setHeader("Szint");
        grid.addColumn(new ComponentRenderer<>( room ->{
            Button aboutButton = new Button("About");
            aboutButton.addClickListener( clickEvent ->{
                UI.getCurrent().navigate("room/view/" + room.getId());
            });
            return aboutButton;
        }));

        grid.asSingleSelect().addValueChangeListener(gridRoomComponentValueChangeEvent -> {
            form.setVisible(true);
            form.setRoom(gridRoomComponentValueChangeEvent.getValue());
            form.setReloader(this);
        });

        Button button = new Button();
        button.setText("Add");
        button.setIcon(VaadinIcon.PLUS.create());
        button.addClickListener(buttonClickEvent -> {
            form.setVisible(true);
            form.setReloader(this);
            form.setRoom(new Room());
        });

        add(grid);
        add(button);
        add(form);
    }

    @Override
    public void reload() {
        grid.setItems(repository.findAll());
    }
}
